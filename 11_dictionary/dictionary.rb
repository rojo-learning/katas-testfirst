
class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(term)
    case term
    when String then @entries[term] = nil
    when Hash   then term.each { |key, value| @entries[key] = value }
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(term)
    @entries.include? term
  end

  def find(prefix)
    @entries.select { |key, value| key =~ /\A#{prefix}\w*/ }
  end

  def printable
    text = ''
    @entries.sort.each { |key, value| text += "[#{key}] \"#{value}\"\n" }
    text.chomp
  end
end
