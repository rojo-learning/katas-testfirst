
module ArrayExtensions
  def sum
    self.reduce(0, :+)
  end

  def square
    self.map { |e| e * e }
  end

  def square!
    self.replace square
  end
end

class Array
  prepend ArrayExtensions
end
