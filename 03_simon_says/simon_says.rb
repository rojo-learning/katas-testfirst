
# Returns a given string.
def echo(words)
  words
end

# Uppercases a given string.
def shout(words)
  words.upcase
end

# Repeats a string a given number of times.
def repeat(words, multiplier = 2)
  ((words + ' ') * multiplier).chomp(' ')
end

# Returns the first n characters of a string.
def start_of_word(words, n)
  words[0,n]
end

# Returns the first word of a given string.
def first_word(words)
  words.split.first
end

# Formats a given string as a title
def titleize(words)
  title = words.capitalize.split.map do |word|
    %w(the and over).include?(word) ? word : word.capitalize
  end

  title.join(' ')
end
