
module FixnumExtensions
  def in_words
    english_number self
  end

  private
  def english_number(number)
    return 'minus' + english_number(number * -1) if number < 0
    return 'zero' if number == 0

    num_string = ''  #  This is the string we will return.

    # Section for numbers starting from 100 and above #########################

    # If number is below 100, we do not jump here
    if number > 99
      # An array of arrays to map labels from hundreds to... whatever you want.
      # Each inner array is a pair of a number written in exponential notation
      # (1e2 is the same that 1 followed by two zeros or 100) and the name that
      # corresponds to its magnitude. Whit this, more magnitudes can be easily
      # added. to_i is used to work with integers only.

      labels = [
        [1e12.to_i, 'trillion'], [1e9.to_i, 'billion'], [1e6.to_i, 'million'],
        [1e3.to_i, 'thousand'], [1e2.to_i, 'hundred']
      ]

      # "left" is how much of the number is left to write out.
      # "write" is the part being written out right now.
      # write and left... get it?  :)

      left = number

      # Starting from the highest magnitude in "labels", each one is tested to
      # see if its label can be written.

      labels.each do | label |
        if left / label[0] > 0
          # Get the part to be written...
          write = left / label[0]
          # ...and remove it from from the remaining
          left  = left - write * label[0]

          # Make a recursive call for the part to be written and append the
          # magnitude label to it, so it can handle "999 trillions" and so on.
          num_string = num_string + english_number(write) + ' ' + label[1]

          # If there's something left, process and clean it.
          if left > 0
            num_string = num_string + ' ' + english_number(left)
            left       = 0
          end
        end
      end
    else
    # Section for numbers below 100 ###########################################
      # small_English_number is called to handle these numbers
      num_string = num_string + small_english_number(number)
    end

    # Now we just return "num_string"...
    num_string
  end


  # This method will only handle the numbers below 100, so we'll have a less
  # messy code, don't we? :)
  def small_english_number number
    num_string = ''  # This is the string we will return.

    # Arrays to map numbers from 1 to 99
    ones_place = ['one',     'two',       'three',    'four',     'five',
                  'six',     'seven',     'eight',    'nine'                ]
    tens_place = ['ten',     'twenty',    'thirty',   'forty',    'fifty',
                  'sixty',   'seventy',   'eighty',   'ninety'              ]
    teenagers  = ['eleven',  'twelve',    'thirteen', 'fourteen', 'fifteen',
                  'sixteen', 'seventeen', 'eighteen', 'nineteen'            ]

    # Tens section ############################################################
    left  = number
    write = left / 10       # How many tens left to write out?
    left  = left - write*10 # Subtract off those tens.

    if write > 0
      if ((write == 1) and (left > 0))
        # Can't write "tenty-two" for "twelve", so make a special exception.
        # The "-1" is because teenagers[3] is 'fourteen', not 'thirteen'.
        num_string = num_string + teenagers[left - 1]

        # Since units digit was already addressed, there is nothing left.
        left = 0
      else
        # The "-1" is because tens_place[3] is 'forty', not 'thirty'.
        num_string = num_string + tens_place[write - 1]
      end

      if left > 0
        # So we don't write 'sixtyfour' but 'sixty four'
        num_string = num_string + ' '
      end
    end

    # Ones section ############################################################
    write = left  # How many ones left to write out?
    left  = 0     # Subtract off those ones.

    if write > 0
      # The "-1" is because ones_place[3] is 'four', not 'three'.
      num_string = num_string + ones_place[write - 1]
    end

    # Now we just return "num_string"...
    num_string
  end
end

class Fixnum
  prepend FixnumExtensions
end
