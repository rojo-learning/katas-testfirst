
class RPNCalculator
  def initialize
    @stack = []
  end

  def value
    @stack.last
  end

  def push(n)
    @stack.push(n)
  end

  def plus
    @stack.push(get_elements.reduce(:+))
  end

  def minus
    a, b = get_elements
    @stack.push(b - a)
  end

  def times
    @stack.push(get_elements.reduce(:*))
  end

  def divide
    a, b = get_elements
    @stack.push(b / a.to_f)
  end

  def tokens(string)
    string.split.map { |e| (e =~ /\d/) ? e.to_i : e.to_sym }
  end

  def evaluate(string)
    tokens(string).each do |element|
      case element
      when :+ then plus
      when :- then minus
      when :* then times
      when :/ then divide
      else
        push(element)
      end
    end

    value
  end

  private
  def get_elements
    raise 'calculator is empty' if @stack[-2].nil? or @stack[-1].nil?
    @stack.pop(2)
  end
end
