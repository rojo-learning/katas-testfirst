# # Topics
#
# * modules
# * strings
#
# # Pig Latin
# Pig Latin is a made-up children's language that's intended to be confusing. It
# obeys a few simple rules (below) but when it's spoken quickly it's really
# difficult for non-children (and non-native speakers) to understand.
#
# Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end of
#         the word.
# Rule 2: If a word begins with a consonant sound, move it to the end of the
#         word, and then add an "ay" sound to the end of the word.
#
# (There are a few more rules for edge cases, and there are regional variants
#  too, but that should be enough to understand the tests.)
#
# See <http://en.wikipedia.org/wiki/Pig_latin> for more details.

require_relative "pig_latin"
include PigLatin

describe "Translator#translate" do
  it "translates a word beginning with a vowel" do
    expect(translate "apple").to eql "appleay"
  end

  it "translates a word beginning with a consonant" do
    expect(translate "banana").to eql "ananabay"
  end

  it "translates a word beginning with two consonants" do
    expect(translate "cherry").to eql "errychay"
  end

  it "translates a word beginning with three consonants" do
    expect(translate "three").to eql "eethray"
  end

  it "translates two words" do
    expect(translate "eat pie").to eql "eatay iepay"
  end

  it "translates many words" do
    expect(translate "the quick brown fox").to eql "ethay ickquay ownbray oxfay"
  end

  it "counts 'sch' as a single phoneme" do
    expect(translate "school").to eql "oolschay"
  end

  it "counts 'qu' as a single phoneme" do
    expect(translate "quiet").to eql "ietquay"
  end

  it "counts 'qu' as a consonant even when it's preceded by a consonant" do
    expect(translate "square").to eql "aresquay"
  end

  # Test-driving bonus:
  # * Write a test asserting that capitalized words are still capitalized (but
  #   with a different initial capital letter, of course)
  it "capitalizes the translation if the original words were capitalized" do
    expect(translate "The quick brown fox").to eql "Ethay ickquay ownbray oxfay"
  end

  # * Retain the punctuation from the original phrase
  it "places points, dashes and sings in the correct places" do
    expected = "Ethay ickquay, ownbray oxfay."
    expect(translate "The quick, brown fox.").to eql expected
  end
end
