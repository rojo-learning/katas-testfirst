
module PigLatin
  # Takes a word and returns the Pig Latin version of it
  def to_pig_latin(s)
    starts_with_vowel = /\A[aeiouy]/

    until s =~ starts_with_vowel
      (s[0,2].eql? "qu") ? s += s.slice!(0,2) : s += s.slice!(0)
    end

    s + "ay"
  end

  # Translates a string to Pig Latin.
  def translate(s)
    ends_with_symbol = /[\.:,;!\?]\Z/

    if s.split.size > 1
      # If the string has several words, translate each one.
      s = s.split.map { |e| translate(e) }.join(' ')
    else
      # Check if the string is capitalized. If so, set a flag.
      recapitalize = true if s[0] == s[0].upcase
      # Check if the string has punctuation to save it for later.
      punctuation = (s.slice!(-1) if s =~ ends_with_symbol) || ''

      s = to_pig_latin(s)
      s.downcase!.capitalize! if recapitalize
      s + punctuation
    end
  end
end
