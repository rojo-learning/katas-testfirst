
class XmlDocument
  def initialize(ident = false)
    @ident_flag  = ident
    @ident_level = 0
  end

  def hello(attrs = {}, &block)
    build_tag('hello', attrs, block)
  end

  private
  def method_missing(method, *args, &block)
    build_tag(method.to_s, args[0] || {}, block)
  end

  def build_tag(tag, attrs, block)
    element  = identation + open_tag(tag) + attributes(attrs)

    element += if block.nil?
      self_close(:empty) + new_line
    else
      partial  = self_close(:filled) + new_line
      @ident_level += 1

      partial += block.call
      @ident_level -= 1

      partial += identation + close_tag(tag) + new_line
    end
  end

  def open_tag(tag)
    "<#{tag}"
  end

  def self_close(type = :empty)
    case type
    when :empty  then '/>'
    when :filled then '>'
    end
  end

  def close_tag(tag)
    "</#{tag}>"
  end

  def attributes(attrs)
    ''.tap { |s| attrs.each { |k, v| s << " #{k}='#{v}'" } }
  end

  def identation(ident_str = '  ')
    (@ident_flag) ? ident_str * @ident_level : ''
  end

  def new_line(new_line_str = "\n")
    (@ident_flag) ? new_line_str : ''
  end
end
