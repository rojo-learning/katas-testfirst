
# Adds two numbers
def add(n, m)
  n + m
end

# Subtracts a number from other
def subtract(n, m)
  n - m
end

# Sums an array of numbers
def sum(numbers)
  numbers.inject(0, :+)
end

# Multiplies two or more numbers
def multiply(*numbers)
  numbers.inject(1, :*)
end

# Raises one number to the power of another
def power(number, exponent)
  exponent.times.inject(1) { |result| result *= number }
end

# Computes the factorial of a number
def factorial(number)
  number <= 0 ? 1 : number * factorial(number - 1)
end
