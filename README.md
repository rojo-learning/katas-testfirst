# Test-First Teaching - Ruby
A series of labs that aim to teach concepts of the Ruby programming
language using the Test Driven Development approach. More information about this
course can be found on [TestFirst.org](http://testfirst.org/learn_ruby).

## Exercises
- **Hello**: Basic Ruby function syntax.
- **Temperature**: Functions and floating-point math.
- **Calculator**: Functions, math, arrays, iteration.
- **Simon Says**: Functions, strings, default values.
- **Pig Latin**: Modules, strings.
- **Silly Blocks**: Blocks, closures, yield, loops.
- **Performance Monitor**: Stubs, blocks, yield.
- **Hello Friend**: Basic Ruby object syntax.
- **Temperature Object**: Factory method pattern, floating point math, objects,
  constructors, class methods, options hashes.
- **Book Titles**: Classes, objects, instance variables, setters and strings.
- **Timer**: Classes, instance variables, string formats, modular arithmetics.
- **Dictionary**: Hash, array, instance variables, regular expressions.
- **RPN Calculator**: Arrays, arithmetic, strings.
- **XML Document**: Builder pattern, nested closures, method_missing, blocks,
    strings
- **Array Extensions**: Reopening classes, modules, objects, methods.
- **In Words**: Reopening classes, modules, numbers, strings.

---
This repository contains test specs and documentation of the TestFirst.org
project and are included here under _fair use_. All the code of included as
answer to the exercises must by considered _public domain_.