
class Temperature
  def initialize(options)
    if options[:f]
      @f = options[:f]
      @c = in_celsius
    elsif options[:c]
      @c = options[:c]
      @f = in_fahrenheit
    end
  end

  def in_celsius
    (defined? @c) ? @c : Temperature.in_celsius(@f)
  end

  def in_fahrenheit
    (defined? @f) ? @f : Temperature.in_fahrenheit(@c)
  end

  def self.from_celsius(t)
    Temperature.new({ c: t })
  end

  def self.from_fahrenheit(t)
    Temperature.new({ f: t })
  end

  def self.ftoc(f)
    (f - 32) * 5 / 9.0
  end

  self.singleton_class.send :alias_method, :in_celsius, :ftoc

  def self.ctof(c)
    (c * 9.0 / 5) + 32
  end

  self.singleton_class.send :alias_method, :in_fahrenheit, :ctof
end

class Celsius < Temperature
  def initialize(t)
    @c = t
    @f = in_fahrenheit
  end
end

class Fahrenheit < Temperature
  def initialize(t)
    @f = t
    @c = in_celsius
  end
end
