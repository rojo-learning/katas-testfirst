
# Reverses each individual word in a string returned by the block.
def reverser
  yield.split.map(&:reverse).join(' ')
end

# Adds the given value to the value returned by the block.
def adder(value = 1)
  yield + value
end

# Calls a passed block a given number of times.
def repeater(n = 1)
  n.times { yield }
end
