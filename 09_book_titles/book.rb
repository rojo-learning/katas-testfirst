
class Book
  attr_reader :title

  ARTICLES     = %w(the a an)
  CONJUNCTIONS = %w(and but or for nor)
  PREPOSITIONS = %w(in of on at to from by)
  EXCEPTIONS   = ARTICLES + CONJUNCTIONS + PREPOSITIONS

  def title=(string)
    @title = Book.titleize string
  end

  def self.titleize(str)
    aux = str.split.map { |w| (EXCEPTIONS.include? w) ? w : w.capitalize }
    aux.tap { |s| s[0].capitalize! }.join(' ')
  end
end
