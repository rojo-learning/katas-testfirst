
# Finds the average time to execute a block a given number of times.
def measure(cycles = 1)
  (Time.now.tap { cycles.times { yield } } - Time.now).abs / cycles
end
