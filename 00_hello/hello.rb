
# Method that greets.
def hello
  "Hello!"
end

# Method that greets someone.
def greet(who)
  hello.chop + ", #{who}!"
end
