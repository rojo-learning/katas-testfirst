
class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hrs = @seconds / 3600
    min = (@seconds - 3600 * hrs) / 60
    sec = @seconds % 60

    "#{padded hrs}:#{padded min}:#{padded sec}"
  end

  def padded(int)
    int < 10 ? '0' + int.to_s : int.to_s
  end
end
